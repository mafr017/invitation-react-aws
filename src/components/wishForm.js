import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { useForm } from 'react-hook-form';

export default function WishForm(props) {
    //HOOKS
    const [isSuccess, isSuccessSet] = useState(false);
    const [dataTable, dataTableSet] = useState([])
    const { register, handleSubmit, reset, formState: { errors } } = useForm();

    //FUNCTIONS
    const onSubmit = data => {
        axios.post("https://app.fmyinvitation.com/api/v1/invtation", {
            wedding: props.wedding,
            name: data.Name,
            wish: data.Wish
        }).then((response) => {
            if (response?.data?.status === 200) {
                console.log("SUCCESS");
                isSuccessSet(true);
                reset();
            }
        }).catch((err) => {
            console.log(err);
            isSuccessSet(false);
            reset();
        })
        setTimeout(() => {
            isSuccessSet(false);
            getData();
        }, 3000);
    }

    const getData = () => {
        axios.get("https://app.fmyinvitation.com/api/v1/invtation/" + props.wedding)
            .then((response) => {
                if (response?.data?.status === 200) {
                    console.log(response.data);
                    if (response?.data?.data) {
                        dataTableSet(response?.data?.data)
                    }
                }
            }).catch((err) => {
                console.log(err);
            })
    }

    useEffect(() => {
        getData();
    }, []);

    return (
        <div className='mt-8'>
            <form onSubmit={handleSubmit(onSubmit)} className='flex flex-col gap-4'>
                <input type="text" placeholder="Name" {...register("Name", { required: true })}
                    className='px-4 py-2 rounded-2xl' />
                <textarea {...register("Wish", { required: true })}
                    placeholder='Wish'
                    className='px-4 py-2 rounded-2xl' />

                <button type="submit" className='px-2 py-2 w-24 rounded-2xl text-white text-md' style={{ backgroundColor: "#666666" }}>Send</button>
            </form>
            {isSuccess ?
                <div className="text-green-800 font-semibold palatino text-2xl mt-[30px]">
                    Thanks for your wish!
                </div>
                : null
            }
            <div className="flex flex-col mt-8 text-start">
                <span className="border-b-2 border-black w-full"></span>
                <div className="h-[350px] w-full border-zinc-800 overflow-y-scroll">
                    {dataTable.map((data, i) => (
                        <div key={i}>
                            <div className="flex flex-col my-[10px] w-full">
                                <div className="font-bold ml-[5px] pr-[20px] mb-[10px]">
                                    {data.name}
                                </div>
                                <div className="ml-[5px] pr-[20px] whitespace-pre-line">
                                    {data.wish}
                                </div>
                                <div className="my-[10px] ml-[5px] pr-[20px] font-['Calibri']">
                                    {data.createdDate}
                                </div>
                                <span className="border-b-2 border-black w-full"></span>
                            </div>
                        </div>
                    ))}
                </div>
                <span className="border-b-2 border-black w-full"></span>
            </div>
        </div>
    );
}
