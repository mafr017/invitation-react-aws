import React from 'react'
import { useLoaderData, useParams } from 'react-router-dom';

export async function loader() {
    await new Promise((r) => setTimeout(r, 500));
    return "I came from the About.tsx loader function!";
}

export function Component() {
    let data = useLoaderData();
    let params = useParams();
    return (
        <div>
            <h2>About</h2>
            <p>{data}</p>
            <p>param: {params.id}</p>
        </div>
    );
}

Component.displayName = "AboutPage";
