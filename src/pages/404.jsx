import React from 'react'

export default function Page404() {
    return (
        <div className="flex flex-col justify-center h-screen w-screen text-center">
            <h1 className="text-6xl font-semibold text-gray-700 dark:text-gray-200">404</h1>
            <p className="text-gray-700 dark:text-gray-300">
                Page not found.
            </p>
        </div>
    )
}
