import React, { useEffect, useRef, useState } from 'react'
import { useParams } from "react-router-dom";
import { motion, AnimatePresence } from "framer-motion";
import 'swiper/css'

import bg1_2 from '../../assets/img/adi-anggi/attribute1.png'
import imgCover from '../../assets/img/anggi-adi/01_cover.jpg'
import bg1_3 from '../../assets/img/adi-anggi/attribute4.png'
import imgMempelai from '../../assets/img/anggi-adi/best-moment/01.jpg'
import bunga1 from '../../assets/img/adi-anggi/bunga1.png'
import bunga2 from '../../assets/img/adi-anggi/bunga2.png'
import garis1 from '../../assets/img/adi-anggi/gambar1.png'
import garis2 from '../../assets/img/adi-anggi/gambar2.png'

import igIcon from '../../assets/img/instagram.png'
import briIcon from '../../assets/img/bri.png'
import danaIcon from '../../assets/img/dana.png'
import shopepayIcon from '../../assets/img/shopepay.png'
import fmyIcon from '../../assets/img/fmy.png'
import wLoveIcon from '../../assets/img/with-love.png'
import phoneIcon from '../../assets/img/phone.jpg'
import PlayIcon from '../../assets/play-solid.svg'
import PauseIcon from '../../assets/pause-solid.svg'
import lagu from '../../assets/sound2.mp3'

import { useCountdown } from '../../utils/countDown';
import { productImages2 } from '../../assets/img/anggi-adi';
import ProductImagesSlider from '../../components/images-slider';
import WishForm from '../../components/wishForm';
import DocumentMeta from 'react-document-meta';


const useConstructor = (callBack = () => { }) => {
    const hasBeenCalled = useRef(false);
    if (hasBeenCalled.current) return;
    callBack();
    hasBeenCalled.current = true;
}

export function Component() {
    //VARIABLES
    const title = "The Wedding of Adi & Anggi | FMY Invitation"
    const desc = "Selasa, 08 Mei 2023"
    let params = useParams();
    let tamu = params.guest;
    const laguu = useRef(new Audio(lagu));

    const meta = {
        title: title,
        description: desc,
        link: {
            rel: {
                canonical: 'https://fmyinvitation.com/wedding/adi-&-anggi/' + tamu.toString()
            }
        },
        extend: true
    };

    //HOOKS
    const [windowSize, windowSizeSet] = useState({ width: window.innerWidth, height: window.innerHeight });
    const [showModal, showModalSet] = useState(true);
    const [days, hours, minutes, seconds] = useCountdown("2023-05-08");
    const [isPlay, isPlaySet] = useState(true);


    //FUNCTIONS
    useConstructor(() => {
        console.log(
            "Developed by MAFR. Check this out https://github.com/mafr017 !"
        );
        document.title = title;
        document.querySelector('meta[name="description"]').setAttribute("content", title);
    });

    const playHandler = () => {
        isPlaySet((e) => !e)
        if (laguu.current !== null) {
            if (isPlay) {
                laguu.current.pause()
            } else {
                laguu.current.play()
            }
            laguu.current.loop = true;
        }
    }

    const getDimension = () => {
        windowSizeSet({ width: window.innerWidth, height: window.innerHeight });
        // console.log(window.innerWidth + ", " + window.innerHeight);
    }

    useEffect(() => {
        window.addEventListener('resize', getDimension);
        playHandler();
    }, []);

    return (
        <DocumentMeta {...meta}>
            {
                // windowSize.width <= 614 ?
                <>
                    <div className='flex flex-row justify-center'>
                        {windowSize.width <= 614 ? null : <div className='w-[50%] h-full'></div>}
                        <div className={showModal ? 'max-w-[614px] fixed overflow-hidden' : 'max-w-[614px] overflow-hidden'} style={{ backgroundColor: showModal ? "" : "#F1EECF" }}>

                            {showModal ? <div className={"relative h-screen w-screen max-w-[614px]"}></div> : <>
                                {/* CONTENT INVITATION */}
                                <div id='Page1'>
                                    <div className={"relative min-h-[500px] h-[90vh] max-h-[600px] w-full"} style={{ backgroundPosition: 'top right' }}>
                                        <motion.img src={garis1} className='right-4 absolute max-h-[400px] mt-6'
                                            initial={{ opacity: 0, x: "50%" }} whileInView={{ opacity: 1, x: 0, transition: { duration: 0.5, type: "spring", stiffness: 50 } }} />
                                        <div className='flex flex-col justify-center h-full'>
                                            <div className='mx-auto px-10 max-h-[380px] w-full absolute'>
                                                <div className='flex flex-col justify-end absolute h-full'>
                                                    <motion.img src={bunga1} className='w-[30%] m-1 z-50'
                                                        initial={{ opacity: 0, y: "-100px" }} whileInView={{ opacity: 1, y: 0, transition: { duration: 0.5, type: "spring", stiffness: 50 } }} />
                                                </div>
                                                <div className='flex justify-center'>
                                                    <div className='flex flex-col justify-center absolute h-full'>
                                                        <motion.div className="text-start text-white font-['Andershon'] mb-10 z-50"
                                                            initial={{ opacity: 0, y: "-100px" }} whileInView={{ opacity: 1, y: 0, transition: { duration: 0.5, type: "spring", stiffness: 50 } }}>
                                                            <motion.div className='text-6xl z-50'>Adi</motion.div>
                                                            <motion.div className='mt-2 flex text-end justify-center z-50'>
                                                                <motion.div className='text-3xl w-full ml-16 z-50' >and</motion.div>
                                                                <div className='flex justify-end w-full'>
                                                                    <div className='flex flex-col justify-end'>
                                                                        <motion.div className='text-6xl pt-6 text-end z-50'>Anggi</motion.div>
                                                                    </div>
                                                                </div>
                                                            </motion.div>
                                                        </motion.div>
                                                    </div>
                                                </div>
                                                <img src={bg1_2} className='h-full max-h-[380px] w-full -z-50' alt='' />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id='Page2'>
                                    <div className='h-auto'>
                                        <div className='p-8 relative z-50' style={{ backgroundColor: "#DFDBB9" }}>
                                            <div className='bg-white bg-opacity-20 p-8 rounded-[5rem]'>
                                                <motion.div className="text-[0.8rem] font-['Calibri']"
                                                    initial={{ opacity: 0, y: "-100px" }} whileInView={{ opacity: 1, y: 0, transition: { duration: 0.5, type: "spring", stiffness: 50 } }}>
                                                    Tanpa mengurangi rasa hormat, kami memberikan
                                                    kabar bahagia ini untuk memohon do’a dan restu
                                                    dari rekan sekalian. Kami akan menyelenggarakan
                                                    akad dan resepsi pernikahan kami.
                                                </motion.div>
                                                <motion.div initial={{ opacity: 0, y: "-100px" }} whileInView={{ opacity: 1, y: 0, transition: { duration: 0.5, type: "spring", stiffness: 50 } }}>
                                                    <div className="mt-6 text-base font-black font-['Aboreto']" style={{ color: "#723E00" }}>
                                                        Bripda Adi Maulana Syafri
                                                    </div>
                                                    <div className='flex flex-row gap-2'>
                                                        <img src={igIcon} className='mt-1 w-4 h-4' alt="igIcon" />
                                                        <a href='https://instagram.com/adimaulana_s?igshid=YmMyMTA2M2Y='
                                                            className='text-xs my-auto'>adimaulana_s</a>
                                                    </div>
                                                    <div className="mt-1 text-xs font-['Calibri']">Putra dari :</div>
                                                    <div className="mt-1 text-xs font-['Calibri']">Bapak Hamid & Ibu Etti, S.Pd</div>
                                                </motion.div>
                                                <motion.div className="mt-4 mb-6 text-3xl font-['Andershon']" style={{ color: "#723E00" }}
                                                    initial={{ opacity: 0, y: "-100px" }} whileInView={{ opacity: 1, y: 0, transition: { duration: 0.5, type: "spring", stiffness: 50 } }}>
                                                    dengan
                                                </motion.div>
                                                <motion.div
                                                    initial={{ opacity: 0, y: "-100px" }} whileInView={{ opacity: 1, y: 0, transition: { duration: 0.5, type: "spring", stiffness: 50 } }}>
                                                    <div className="text-base font-black font-['Aboreto']" style={{ color: "#723E00" }}>
                                                        Siti AnggraeNi Nur Alifa, S.Pd
                                                    </div>
                                                    <div className='flex flex-row gap-2'>
                                                        <img src={igIcon} className='mt-1 w-4 h-4' alt="igIcon" />
                                                        <a href='https://instagram.com/anggianggraa?igshid=YmMyMTA2M2Y='
                                                            className="text-xs my-auto font-['Calibri']">anggianggraa</a>
                                                    </div>
                                                    <div className="mt-1 text-xs font-['Calibri']">Putri dari :</div>
                                                    <div className="mt-1 text-xs font-['Calibri']">Bapak Dedi Supriyadi, S.Pd & Ibu Atun Arhatun</div>
                                                </motion.div>
                                                <motion.div className='mt-4'
                                                    initial={{ opacity: 0, y: "-100px" }} whileInView={{ opacity: 1, y: 0, transition: { duration: 0.5, type: "spring", stiffness: 50 } }}>
                                                    <img src={imgMempelai} className='rounded-3xl' alt="imgMempelai" />
                                                </motion.div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id='Page3'>
                                    <div className='pt-8 relative z-50' style={{ backgroundColor: "#F1EECF" }}>
                                        <div className='pl-8 flex flex-row'>
                                            <motion.div className="mr-6 text-lg font-black uppercase text-center w-fit font-['Aboreto']"
                                                initial={{ opacity: 0, x: "-50%" }} whileInView={{ opacity: 1, x: 0, transition: { duration: 0.5, type: "spring", stiffness: 50 } }}>
                                                Save<br />The<br />Date
                                            </motion.div>
                                            <div className="py-8 pl-8 my-8 text-white font-['Century751BT']" style={{ backgroundColor: "#ACA77B" }}>
                                                <motion.div
                                                    initial={{ opacity: 0, x: "50%" }} whileInView={{ opacity: 1, x: 0, transition: { duration: 0.5, type: "spring", stiffness: 50 } }}>
                                                    Akad Nikah (Telah dilaksanakan)
                                                </motion.div>
                                                <motion.div className='my-4 text-sm pr-8'
                                                    initial={{ opacity: 0, x: "50%" }} whileInView={{ opacity: 1, x: 0, transition: { duration: 0.5, type: "spring", stiffness: 50 } }}>
                                                    Selasa, 02 Mei 2023<br />
                                                    08.00 - 10.00 WIB
                                                </motion.div>
                                                <motion.div className='text-xs pr-8'
                                                    initial={{ opacity: 0, x: "50%" }} whileInView={{ opacity: 1, x: 0, transition: { duration: 0.5, type: "spring", stiffness: 50 } }}>
                                                    Blok Kliwon Rt.002 Rw.004, Desa Mekarsari<br />
                                                    Kec. Jatiwangi,  Kab. Majalengka
                                                </motion.div>
                                            </div>
                                        </div>
                                        <div className=''>
                                            <motion.img src={garis2} className='w-16 absolute mt-6 right-2 bottom-0'
                                                initial={{ opacity: 0, x: "50%" }} whileInView={{ opacity: 1, x: 0, transition: { duration: 0.5, type: "spring", stiffness: 50 } }} />
                                            <div className='flex justify-end relative mr-24 z-50'>
                                                <div className="text-end text-white font-['Century751BT'] absolute z-50 pt-4">
                                                    <motion.div className='px-4'
                                                        initial={{ opacity: 0, x: "-50%" }} whileInView={{ opacity: 1, x: 0, transition: { duration: 0.5, type: "spring", stiffness: 50 } }}>Ngunduh Mantu</motion.div>
                                                    <motion.div className='my-4 text-sm px-4'
                                                        initial={{ opacity: 0, x: "-50%" }} whileInView={{ opacity: 1, x: 0, transition: { duration: 0.5, type: "spring", stiffness: 50 } }}>
                                                        Senin, 08 Mei 2023<br />
                                                        08.00 - 10.00 WIB
                                                    </motion.div>
                                                    <motion.div className='text-xs px-4'
                                                        initial={{ opacity: 0, x: "-50%" }} whileInView={{ opacity: 1, x: 0, transition: { duration: 0.5, type: "spring", stiffness: 50 } }}>
                                                        Blok D1 No.15 Rt.03 Rw.07, Desa Purwawinangun,<br />
                                                        Puri celancang, Kec. Suranenggala, Kab. Cirebon
                                                    </motion.div>
                                                    <div className='w-full h-20 absolute -z-50 -mt-10' style={{ backgroundColor: "#ACA77B" }}></div>
                                                    <div id='TimerDateWedding' className='mt-4 w-full mb-8 pl-8' style={{ backgroundColor: "#ACA77B" }}>
                                                        <motion.div id='TimerAngka' className='grid grid-cols-4 text-sm font-semibold'
                                                            initial={{ opacity: 0, x: "-50%" }} whileInView={{ opacity: 1, x: 0, transition: { duration: 0.5, type: "spring", stiffness: 50 } }}>
                                                            <div className='text-center'>
                                                                <div>{days}</div>
                                                                <div>Hari</div>
                                                            </div>
                                                            <div className='text-center'>
                                                                <div>{hours}</div>
                                                                <div>Jam</div>
                                                            </div>
                                                            <div className='text-center'>
                                                                <div>{minutes}</div>
                                                                <div>Menit</div>
                                                            </div>
                                                            <div className='text-center'>
                                                                <div>{seconds}</div>
                                                                <div>Detik</div>
                                                            </div>
                                                        </motion.div>
                                                        <div className='flex justify-end pr-8 py-4'>
                                                            <motion.div className='w-[60%] h-[0.1rem] bg-white rounded-2xl'
                                                                initial={{ opacity: 0, x: "-50%" }} whileInView={{ opacity: 1, x: 0, transition: { duration: 0.5, type: "spring", stiffness: 50 } }}></motion.div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src={bg1_3} className='h-full max-h-[380px] w-full -z-50 mb-32' alt='' />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id='Page3.1'>
                                    <div className='h-auto'>
                                        <div className='py-8 relative z-50' style={{ backgroundColor: "#DFDBB9" }}>
                                            <motion.div className="text-4xl text-center font-['Andershon']"
                                                initial={{ opacity: 0, y: "-50px" }} whileInView={{ opacity: 1, y: 0, transition: { duration: 0.5, type: "spring", stiffness: 50 } }}>
                                                Best   Moment
                                            </motion.div>
                                            <div className='px-8 py-4'>
                                                <ProductImagesSlider images={productImages2} />
                                            </div>
                                            <div className='flex justify-center'>
                                                <motion.div className='my-8 w-[60%] h-[0.1rem] bg-black rounded-2xl'
                                                    initial={{ opacity: 0, y: "-50px" }} whileInView={{ opacity: 1, y: 0, transition: { duration: 0.5, type: "spring", stiffness: 50 } }}></motion.div>
                                            </div>
                                            <motion.div className="my-8 text-4xl text-center font-['Andershon']"
                                                initial={{ opacity: 0, y: "-50px" }} whileInView={{ opacity: 1, y: 0, transition: { duration: 0.5, type: "spring", stiffness: 50 } }}>
                                                Special Guest
                                            </motion.div>
                                            <div className="mt-16 my-8 text-sm text-center font-['Calibri']">
                                                <motion.div className='font-semibold'
                                                    initial={{ opacity: 0, y: "-50px" }} whileInView={{ opacity: 1, y: 0, transition: { duration: 0.5, type: "spring", stiffness: 50 } }}>1. KOMBESPOL ARIF BUDIMAN, S.I.K., M.H.</motion.div>
                                                <motion.div
                                                    initial={{ opacity: 0, y: "-50px" }} whileInView={{ opacity: 1, y: 0, transition: { duration: 0.5, type: "spring", stiffness: 50 } }}>(Kapolresta Cirebon)</motion.div>
                                                <motion.div className='font-semibold'
                                                    initial={{ opacity: 0, y: "-50px" }} whileInView={{ opacity: 1, y: 0, transition: { duration: 0.5, type: "spring", stiffness: 50 } }}>2. KOMPOL ANTON, S.H., S.I.K., M.H.</motion.div>
                                                <motion.div
                                                    initial={{ opacity: 0, y: "-50px" }} whileInView={{ opacity: 1, y: 0, transition: { duration: 0.5, type: "spring", stiffness: 50 } }}>(Kasat Reskim Polresta Cirebon)</motion.div>
                                                <motion.div className='font-semibold'
                                                    initial={{ opacity: 0, y: "-50px" }} whileInView={{ opacity: 1, y: 0, transition: { duration: 0.5, type: "spring", stiffness: 50 } }}>3. H.TARWADI, S.Pd., M.M.Pd</motion.div>
                                                <motion.div
                                                    initial={{ opacity: 0, y: "-50px" }} whileInView={{ opacity: 1, y: 0, transition: { duration: 0.5, type: "spring", stiffness: 50 } }}>(Korwil Bid. Dik. Cam. Gunungjati)</motion.div>
                                                <motion.div className='font-semibold'
                                                    initial={{ opacity: 0, y: "-50px" }} whileInView={{ opacity: 1, y: 0, transition: { duration: 0.5, type: "spring", stiffness: 50 } }}>4. H. SUMA, S.Ag., M.M.Pd.</motion.div>
                                                <motion.div
                                                    initial={{ opacity: 0, y: "-50px" }} whileInView={{ opacity: 1, y: 0, transition: { duration: 0.5, type: "spring", stiffness: 50 } }}>(Kepala SDN 1 Mayung)</motion.div>
                                            </div>
                                            <div className='flex justify-center'>
                                                <motion.div className='my-8 w-[60%] h-[0.1rem] bg-black rounded-2xl'
                                                    initial={{ opacity: 0, y: "-50px" }} whileInView={{ opacity: 1, y: 0, transition: { duration: 0.5, type: "spring", stiffness: 50 } }}></motion.div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id='Page4'>
                                    <div className='h-auto'>
                                        <div className='py-8 relative z-50 text-center' style={{ backgroundColor: "#F1EECF" }}>
                                            <motion.div className="text-4xl text-center font-['Andershon']"
                                                initial={{ opacity: 0, y: "-50px" }} whileInView={{ opacity: 1, y: 0, transition: { duration: 0.5, type: "spring", stiffness: 50 } }}>
                                                Map
                                            </motion.div>
                                            <motion.div className="mt-4 text-xs text-center font-['Calibri']"
                                                initial={{ opacity: 0, y: "-50px" }} whileInView={{ opacity: 1, y: 0, transition: { duration: 0.5, type: "spring", stiffness: 50 } }}>
                                                Blok D1 No.15 Rt.03 Rw.07, Desa Purwawinangun,<br />
                                                Puri celancang, Kec. Suranenggala, Kab. Cirebon
                                            </motion.div>
                                            <div className='px-4 py-8'>
                                                <iframe title='gMaps' src="https://www.google.com/maps/embed?pb=!1m17!1m12!1m3!1d3963.0246104512908!2d108.53041931477124!3d-6.643865595195887!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m2!1m1!2zNsKwMzgnMzcuOSJTIDEwOMKwMzEnNTcuNCJF!5e0!3m2!1sid!2sid!4v1682660450438!5m2!1sid!2sid" width="100%" height="500" style={{ border: 0 }} loading="lazy"></iframe>
                                            </div>
                                            <motion.button
                                                className="text-black text-base px-10 py-3 rounded-full shadow hover:shadow-lg mr-1 mb-1 ease-linear transition-all duration-150 font-serif"
                                                style={{ backgroundColor: "#ACA77B" }}
                                                type="button"
                                                onClick={() => window.location.replace("https://goo.gl/maps/K4wbnBtiES2jQKLr7")}
                                                initial={{ opacity: 0, y: "-50px" }} whileInView={{ opacity: 1, y: 0, transition: { duration: 0.5, type: "spring", stiffness: 50 } }}>
                                                Lihat Lokasi
                                            </motion.button>
                                            <div className='flex justify-center'>
                                                <motion.div className='mt-8 w-full h-[0.1rem] bg-black rounded-2xl mx-8'
                                                    initial={{ opacity: 0, y: "-50px" }} whileInView={{ opacity: 1, y: 0, transition: { duration: 0.5, type: "spring", stiffness: 50 } }}></motion.div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id='Page5'>
                                    <div className='h-auto'>
                                        <div className="px-8 pb-8 relative z-50 text-center font-['Calibri']" style={{ backgroundColor: "#F1EECF" }}>
                                            <motion.div className="text-4xl font-['Andershon']"
                                                initial={{ opacity: 0, y: "-50px" }} whileInView={{ opacity: 1, y: 0, transition: { duration: 0.5, type: "spring", stiffness: 50 } }}>
                                                Gift
                                            </motion.div>
                                            <motion.div className='mt-4 text-xs'
                                                initial={{ opacity: 0, y: "-50px" }} whileInView={{ opacity: 1, y: 0, transition: { duration: 0.5, type: "spring", stiffness: 50 } }}>
                                                Jika berkenan memberikan hadiah personal, bisa<br />dikirim melalui rekening berikut
                                            </motion.div>
                                            <motion.div
                                                initial={{ opacity: 0, y: "-50px" }} whileInView={{ opacity: 1, y: 0, transition: { duration: 0.5, type: "spring", stiffness: 50 } }}>
                                                <div className='mt-6 w-full flex flex-row justify-center'>
                                                    <img src={briIcon} className='w-1/2' alt='briIcon' />
                                                </div>
                                                <div className='mt-2 text-xs'>
                                                    No rekening : 040701021993509<br />a/n. ADI MAULANA S
                                                </div>
                                            </motion.div>
                                            <motion.div
                                                initial={{ opacity: 0, y: "-50px" }} whileInView={{ opacity: 1, y: 0, transition: { duration: 0.5, type: "spring", stiffness: 50 } }}>
                                                <div className='mt-6 w-full flex flex-row justify-center'>
                                                    <img src={danaIcon} className='w-1/2' alt='danaIcon' />
                                                </div>
                                                <div className='mt-2 text-xs'>
                                                    082320008956
                                                </div>
                                            </motion.div>
                                            <motion.div
                                                initial={{ opacity: 0, y: "-50px" }} whileInView={{ opacity: 1, y: 0, transition: { duration: 0.5, type: "spring", stiffness: 50 } }}>
                                                <div className='mt-6 w-full flex flex-row justify-center'>
                                                    <img src={shopepayIcon} className='w-1/2' alt='shopepayIcon' />
                                                </div>
                                                <div className='mt-2 text-xs'>
                                                    089660891685
                                                </div>
                                            </motion.div>
                                            <div className='flex justify-center'>
                                                <motion.div className='mt-8 w-full h-[0.1rem] bg-black rounded-2xl'
                                                    initial={{ opacity: 0, y: "-50px" }} whileInView={{ opacity: 1, y: 0, transition: { duration: 0.5, type: "spring", stiffness: 50 } }}></motion.div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id='Page6'>
                                    <div className='h-auto'>
                                        <div className='pb-16 pt-4 relative z-50 text-center' style={{ backgroundColor: "#DFDBB9" }}>
                                            <motion.div className="px-8 mt-6 text-4xl font-['Andershon']"
                                                initial={{ opacity: 0, y: "-50px" }} whileInView={{ opacity: 1, y: 0, transition: { duration: 0.5, type: "spring", stiffness: 50 } }}>
                                                Join with us
                                            </motion.div>
                                            <motion.div className="px-8 mt-4 text-xs font-['Calibri']"
                                                initial={{ opacity: 0, y: "-50px" }} whileInView={{ opacity: 1, y: 0, transition: { duration: 0.5, type: "spring", stiffness: 50 } }}>
                                                Berikan ucapan dan doa terbaik untuk kami.
                                            </motion.div>
                                            <motion.div className='px-8 mt-2 font-serif'
                                                initial={{ opacity: 0, y: "-50px" }} whileInView={{ opacity: 1, y: 0, transition: { duration: 0.5, type: "spring", stiffness: 50 } }}>
                                                <WishForm wedding="adi-&-anggi" />
                                            </motion.div>
                                            <div className="p-8 mt-8 text-center text-xs font-['Calibri']" style={{ backgroundColor: "#cbc8ab" }}>
                                                <motion.div
                                                    initial={{ opacity: 0, y: "-50px" }} whileInView={{ opacity: 1, y: 0, transition: { duration: 0.5, type: "spring", stiffness: 50 } }}>
                                                    <div className='mb-6 w-full flex flex-row justify-center'>
                                                        <img src={bunga2} className='w-[20%]' alt='danaIcon' />
                                                    </div>
                                                </motion.div>
                                                <motion.div
                                                    initial={{ opacity: 0, y: "-50px" }} whileInView={{ opacity: 1, y: 0, transition: { duration: 0.5, type: "spring", stiffness: 50 } }}>
                                                    “Dan diantara tanda-tanda kekuasaan-Nya untukmu pasangan
                                                    hidup dari jenismu sendiri supaya kamu merasa tentram
                                                    disampingnya dan dijadikan-Nya rasa kasih sayang
                                                    diantara kamu. Sesungguhnya yang demikian menjadi
                                                    tanda tanda kebesaran-Nya bagi orang-orang yang berfikir”
                                                    <div className='mt-2'>(Q.S. Ar-Ruum : 21)</div>
                                                </motion.div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id='Page7'>
                                    <div className='h-auto'>
                                        <div className={"relative min-h-[500px] h-[90vh] max-h-[600px] w-full bg-cover bg-[url('/src/assets/img/anggi-adi/02_mempelai.jpg')]"} style={{ backgroundPosition: 'center' }}>
                                            <div className='text-white text-center flex flex-col justify-end h-full'>
                                                <div className='mt-2 w-full flex flex-row justify-center'>
                                                    <img src={wLoveIcon} className='w-full h-[110%]' alt='wLoveIcon' />
                                                </div>
                                                <div className='mt-4 mb-8 text-xs uppercase font-serif tracking-[0.25rem]'>
                                                    ADI & ANGGI
                                                </div>

                                            </div>
                                        </div>
                                        <div className='p-8 text-center text-xs' style={{ backgroundColor: "#F1EECF" }}>
                                            <div className="font-['Calibri']">Powered By :</div>
                                            <a href="https://instagram.com/fmy.invitation?igshid=YmMyMTA2M2Y=">
                                                <div className='mt-2 w-full flex flex-row justify-center'>
                                                    <img src={fmyIcon} className='w-[35%]' alt='fmyIcon' />
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                                {/* Play & Pause Button */}
                                <button onClick={() => playHandler()}>
                                    <div className='fixed z-50 h-10 mb-8 bottom-0'>
                                        <div className='h-10 w-10 ml-8 text-center bg-white rounded-full opacity-50 cursor-pointer hover:opacity-100'>
                                            <img src={isPlay ? PauseIcon : PlayIcon} alt='asdasd' className='h-full w-full p-2' />
                                        </div>
                                    </div>
                                </button>
                            </>}

                            {/* MODAL OPENING INVITATION */}
                            <AnimatePresence>
                                {showModal ? (
                                    <>
                                        <motion.div className="absolute inset-0 z-50 h-full w-full text-white"
                                            exit={{
                                                opacity: 0,
                                                x: '-150vw',
                                                transition: {
                                                    ease: "easeInOut",
                                                    duration: 1.5,
                                                    delay: 0.15
                                                },
                                            }}>
                                            <div className={"relative flex flex-row justify-center text-center h-full"}>
                                                <div className='flex flex-col justify-between h-screen absolute'>
                                                    <div className='pt-10'>
                                                        <motion.div className="text-base font-bold uppercase font-['Calibri']"
                                                            initial={{ opacity: 0, y: "-50px" }} whileInView={{ opacity: 1, y: 0, transition: { duration: 0.5, type: "spring", stiffness: 50 } }}>Wedding Invitation</motion.div>
                                                        <motion.div className="text-4xl mt-2 font-semibold font-['Andershon']"
                                                            initial={{ opacity: 0, y: "-50px" }} whileInView={{ opacity: 1, y: 0, transition: { duration: 0.5, type: "spring", stiffness: 50 } }}>Adi & Anggi</motion.div>
                                                        <motion.div className="mt-4 text-lg font-semibold font-['Calibri']"
                                                            initial={{ opacity: 0, y: "-50px" }} whileInView={{ opacity: 1, y: 0, transition: { duration: 0.5, type: "spring", stiffness: 50 } }}>Kepada Yth :</motion.div>
                                                        <motion.div className='text-lg font-semibold font-serif'
                                                            initial={{ opacity: 0, y: "-50px" }} whileInView={{ opacity: 1, y: 0, transition: { duration: 0.5, type: "spring", stiffness: 50 } }}>Bapak/Ibu/Saudara/i</motion.div>
                                                        <motion.div className='text-xl font-bold font-serif'
                                                            initial={{ opacity: 0, y: "-50px" }} whileInView={{ opacity: 1, y: 0, transition: { duration: 0.5, type: "spring", stiffness: 50 } }}>{tamu}</motion.div>
                                                    </div>
                                                    <div className='pb-24'>
                                                        <button
                                                            className="bg-white text-black font-semibold text-base px-6 py-2 rounded-full shadow hover:shadow-lg mr-1 mb-1 ease-linear transition-all duration-150 font-serif"
                                                            type="button"
                                                            onClick={() => {
                                                                showModalSet(false);
                                                                playHandler();
                                                            }}
                                                        >
                                                            Buka Undangan
                                                        </button>
                                                    </div>
                                                </div>
                                                <img src={imgCover} className='w-full h-screen object-cover -z-50 absolute text-center' alt='' />
                                            </div>
                                        </motion.div>
                                    </>
                                ) : null}
                            </AnimatePresence>

                        </div >
                        {windowSize.width <= 614 ? null : <div className='w-[50%] h-full'></div>}
                    </div >
                </>
                // :
                // <>
                //     <div className='h-screen text-center flex flex-col justify-center px-20'>
                //         <div>
                //             <div className='flex flex-row justify-center'>
                //                 <img src={phoneIcon} className='h-[50vh]' alt='phoneIcon' />
                //             </div>
                //             <div className='text-xl'>Looks like you opened with Desktop or PC or Laptop.<br />Please open with mobile phone!</div>
                //             <div className='text-xl mt-4'>Sepertinya Anda membuka dengan Desktop or PC or Laptop.<br />Silakan buka dengan ponsel!</div>
                //         </div>
                //     </div>
                // </>
            }
        </DocumentMeta>
    );
}