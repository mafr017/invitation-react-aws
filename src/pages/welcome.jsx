import React from 'react'
import { Player } from '@lottiefiles/react-lottie-player';

import fmyIcon from '../assets/img/fmy.png';

export default function Welcome() {
    document.title = "FMY Invitation";
    return (
        <div>
            <center>
                <div className="relative">
                    <div className="fixed top-1/2 left-1/2" style={{ transform: "translate(-50%, -50%)", color: "rgb(89, 63, 45)" }}>
                        <Player className="w-[100%] sm:w-[70%]" autoplay speed={1}
                            src="https://assets1.lottiefiles.com/packages/lf20_8uxiynbc.json" loop>
                        </Player>
                        <p className="font-serif text-xl sm:text-2xl mt-[2%]">
                            Order your online invitaion
                        </p>
                        <a href="https://www.instagram.com/fmy.invitation/"><img className="mt-10 sm:mt-[5%]" src={fmyIcon} style={{ color: "rgb(89, 63, 45)" }} alt='fmyIcon' /></a>
                        <p className="font-serif text-xl sm:text-2xl mt-10 sm:mt-[5%]">
                            If you interested to make an invitaion
                        </p>
                        <a href="https://wa.me/c/6285171517686">
                            <button className="text-white text-xl font-serif font-bold p-3 rounded-2xl mt-3 sm:mt-[5%]"
                                style={{ backgroundColor: "rgb(89, 63, 45)" }}>
                                Contact Us 📨
                            </button>
                        </a>
                    </div>
                </div>
            </center>
        </div>
    )
}
