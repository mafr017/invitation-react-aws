import { RouterProvider, createBrowserRouter } from 'react-router-dom';
import Welcome from './pages/welcome.jsx';
import Page404 from './pages/404.jsx';
import { Player } from '@lottiefiles/react-lottie-player';
import DocumentMeta from 'react-document-meta';
// import Wedding from './pages/wedding.tsx';

const router = createBrowserRouter([
  {
    path: "/",
    children: [
      {
        index: true,
        element: <Welcome />,
      },
      /** CONTOH */
      // {
      //   path: "about",
      //   lazy: () => import("./pages/about.jsx"),
      //   children: [
      //     {
      //       path: ":id",
      //       lazy: () => import("./pages/about.jsx"),
      //     }
      //   ]
      // },
      {
        path: "wedding",
        children: [
          {
            path: "anggi-&-adi/:guest",
            lazy: () => import("./pages/anggi-&-adi/index.jsx"),
          },
          {
            path: "adi-&-anggi/:guest",
            lazy: () => import("./pages/adi-&-anggi/index.jsx"),
          }
        ]
      }
    ]
  },
  {
    path: "*",
    element: <Page404 />,
  },
]);

const meta = {
  title: 'Some Meta Title',
  description: 'I am a description, and I can create multiple tags',
  canonical: 'http://example.com/path/to/page',
  meta: {
    charSet: 'utf-8',
    name: {
      keywords: 'react,meta,document,html,tags'
    },
    itemProp: {
      name: 'The Name or Title Here',
      description: 'This is the page description',
      image: 'https://www.google.com/images/branding/googlelogo/2x/googlelogo_light_color_92x30dp.png'
    },
    property: {
      'og:title': 'I am overriding!',
      'og:type': 'article',
      'og:image': 'https://www.google.com/images/branding/googlelogo/2x/googlelogo_light_color_92x30dp.png',
      'og:site_name': 'Example Site Name',
      'og:price:amount': '19.50',
      'og:price:currency': 'USD',
      'twitter:site': '@site',
      'twitter:title': 'I am a Twitter title'
    }
  },
  auto: {
    ograph: true
  }
};

function App() {
  return (
    <DocumentMeta {...meta}>
      <RouterProvider router={router} fallbackElement={
        <div className="relative">
          <div className="fixed top-1/2 left-1/2 text-center" style={{ transform: "translate(-50%, -50%)" }}>
            <Player autoplay speed={2}
              src="https://assets1.lottiefiles.com/packages/lf20_y1fvviil.json" loop style={{ width: "250px" }}>
            </Player>
            <p className="font-mono text-4xl text-blue-300">Loading</p>
          </div>
        </div>
      } >
        {/* <title>FMY Invitation</title>
        <meta
          name="description"
          content="Order Your Wedding Invitation ❤️!"
        /> */}
      </RouterProvider>
    </DocumentMeta>
  );
}

export default App;
