import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import DocumentMeta from 'react-document-meta';

const root = ReactDOM.createRoot(document.getElementById('root'));
const meta = {
  title: 'Daily Report',
  description: 'A new way of creating Daily Reports',
  // canonical: '',
  meta: {
    charset: 'utf-8',
    name: {
      keywords: 'journal, report, daily report, react'
    }
  }
}

root.render(
  <>
    <DocumentMeta {...meta} />
    <App />
  </>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
