import { useEffect, useState } from 'react';

const useCountdown = (targetDate) => {
    const countDownDate = new Date(targetDate).getTime();

    const [countDown, setCountDown] = useState(
        countDownDate - new Date().getTime()
    );

    useEffect(() => {
        const interval = setInterval(() => {
            setCountDown(countDownDate - new Date().getTime());
        }, 1000);

        return () => clearInterval(interval);
    }, [countDownDate]);

    return getReturnValues(countDown);
};

const getReturnValues = (countDown) => {
    // calculate time left
    var days = Math.floor(countDown / (1000 * 60 * 60 * 24));
    days = days <= 0 ? 0 : days;
    var hours = Math.floor(
        (countDown % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
    );
    hours = hours <= 0 ? 0 : hours;
    var minutes = Math.floor((countDown % (1000 * 60 * 60)) / (1000 * 60));
    minutes = minutes <= 0 ? 0 : minutes;
    var seconds = Math.floor((countDown % (1000 * 60)) / 1000);
    seconds = seconds <= 0 ? 0 : seconds;

    return [days, hours, minutes, seconds];
};

export { useCountdown };